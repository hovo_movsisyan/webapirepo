﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApiRepo.Models;
using WebApiRepo.Unitofwork;

namespace WebApiRepo.Controllers
{
    public class CategoryController : ApiController
    {
        private UnitOfWork _unit;

        public CategoryController()
        {
            _unit = new UnitOfWork(new MarketContext());
        }

        // GET: api/Category
        [HttpGet]
        public IEnumerable<Category> GetAllCategories() => _unit.Category.GetAll();

        // GET: api/Category/5
        [HttpGet]
        public Category GetCategory(int id) => _unit.Category.Get(id);

        // POST: api/Category
        [HttpPost]
        public void AddCategory(Category category)
        {
            _unit.Category.Add(category);
            _unit.Save();
        }
        
        // PUT: api/Category/5
        [HttpPost]
        public void EditCategory(int id, Category category)
        {
             Category _category = _unit.Category.Get(id);

            _category.CategoryName = category.CategoryName;
            _category.Description = category.Description;

            _unit.Category.Update(_category);
            _unit.Save();
        }

        // DELETE: api/Category/5
        [HttpDelete]
        public void DeleteCategory(int id)
        {
            Category category = _unit.Category.Get(id);
            _unit.Category.Remove(category);
            _unit.Save();
        }
    }
}
