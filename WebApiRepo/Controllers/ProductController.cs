﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApiRepo.Models;
using WebApiRepo.Unitofwork;

namespace WebApiRepo.Controllers
{
    public class ProductController : ApiController
    {
        private UnitOfWork _unit;

        public ProductController()
        {
            _unit = new UnitOfWork(new MarketContext());
        }

        // GET: api/Product
        [HttpGet]
        public IEnumerable<Product> GetAllProducts() => _unit.Product.GetAll();

        // GET: api/Product/5
        [HttpGet]
        public Product GetProduct(int id) => _unit.Product.Get(id);

        // POST: api/Product
        [HttpPost]
        public void AddProduct(Product product)
        {
            _unit.Product.Add(product);
            _unit.Save();
        }

        // PUT: api/Product/5
        [HttpPost]
        public void EditProduct(int id, Product product)
        {
            Product _product = _unit.Product.Get(id);

            _product.ProductName = product.ProductName;
            _product.Price = product.Price;
            _product.Description = product.Description;
            _product.CategoryId = product.CategoryId;

            _unit.Product.Update(_product);
            _unit.Save();
        }

        // DELETE: api/Product/5
        [HttpDelete]
        public void DeleteProduct(int id)
        {
            Product product = _unit.Product.Get(id);
            _unit.Product.Remove(product);
            _unit.Save();
        }
    }
}
