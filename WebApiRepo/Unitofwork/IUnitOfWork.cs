﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApiRepo.Repositories;

namespace WebApiRepo.Unitofwork
{
    public interface IUnitOfWork: IDisposable
    {
        ICategoryRepository Category { get; }
        IProductRepository Product { get; }
        int Save();
    }
}
