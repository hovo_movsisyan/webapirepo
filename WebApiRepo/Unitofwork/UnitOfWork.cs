﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApiRepo.Models;
using WebApiRepo.Repositories;

namespace WebApiRepo.Unitofwork
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly MarketContext _context;

        public UnitOfWork(MarketContext context)
        {
            _context = context;
            Category = new CategoryRepository(_context);
            Product = new ProductRepository(_context);
        }

        public ICategoryRepository Category { get; private set; }

        public IProductRepository Product { get; private set; }

        public void Dispose()
        {
           _context.Dispose();
        }

        public int Save() => _context.SaveChanges();
    }
}