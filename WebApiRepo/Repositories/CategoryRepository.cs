﻿
using WebApiRepo.GenericRepositories;
using WebApiRepo.Models;

namespace WebApiRepo.Repositories
{
    public class CategoryRepository:GenericRepository<Category>, ICategoryRepository
    {
        public CategoryRepository(MarketContext context):base(context)
        {

        }
    }
}