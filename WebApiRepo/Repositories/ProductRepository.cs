﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApiRepo.GenericRepositories;
using WebApiRepo.Models;

namespace WebApiRepo.Repositories
{
    public class ProductRepository : GenericRepository<Product>, IProductRepository
    {
        public ProductRepository(MarketContext context):base(context)
        {

        }


    }
}