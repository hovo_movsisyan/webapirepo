﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApiRepo.GenericRepositories;
using WebApiRepo.Models;

namespace WebApiRepo.Repositories
{
    public interface ICategoryRepository : IGenericRepository<Category>
    {
    }
}
