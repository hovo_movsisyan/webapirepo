﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace WebApiRepo.GenericRepositories
{
    public class GenericRepository<T> : IGenericRepository<T> where T : class
    {
        DbContext _context;

        private IDbSet<T> _entities;

        protected IDbSet<T> Entities
        {
            get
            {
                if (_entities == null)
                {
                    _entities = _context.Set<T>();
                }
                return _entities;
            }
        }

        public GenericRepository(DbContext context)
        {
            _context = context;
        }

        public void Add(T entity)
        {
            Entities.Add(entity);
        }

        public T Get(int id) => Entities.Find(id);

        public IEnumerable<T> GetAll() => Entities.ToList();

        public void Remove(T entity)
        {
            Entities.Remove(entity);
        }

        public void Update(T entity)
        {
            Entities.Attach(entity);
            _context.Entry(entity).State = EntityState.Modified;
        }
    }
}