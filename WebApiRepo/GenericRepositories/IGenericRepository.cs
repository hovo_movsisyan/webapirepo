﻿using System.Collections.Generic;

namespace WebApiRepo.GenericRepositories
{
    public interface IGenericRepository<T> where T:class
    {
        T Get(int id);
        IEnumerable<T> GetAll();
        void Add(T entity);
        void Update(T entity);
        void Remove(T entity);
    }
}
